<?php
if (!defined('ABSPATH')) {
    exit;
}
if(!defined('WPPAYFORMHASPRO')) {
    define('WPPAYFORMHASPRO', true);
}

class WPPayFormProInit
{
    public function boot()
    {
        if(is_admin()) {
            $this->adminHooks();
        }
        $this->commonHooks();
    }

    public function commonHooks()
    {
        $scheduleSettings = new \WPPayForm\Pro\Classes\SchedulingSettings();
        $scheduleSettings->checkRestrictionHooks();

        // Register Paypal Payment Gateway
        $paypal = new WPPayForm\Pro\GateWays\PayPal\PayPal();
        $paypal->init();

        // Register Paypal IPN
        $paypalIpn = new \WPPayForm\Pro\GateWays\PayPal\PayPalIpn();
        $paypalIpn->init();

        // Register Stripe Event Handler
        $stripeEvent = new \WPPayForm\Pro\GateWays\Stripe\StripeListener();
        $stripeEvent->init();

        // Register Offline Payment Gateway
        $offline = new WPPayForm\Pro\GateWays\Offline\OfflineProcessor();
        $offline->init();

        // Additional Form Info Handler
        $infoHandler = new \WPPayForm\Pro\Classes\FormAdditionalInfo();
        $infoHandler->register();

        // Email Notification Handler
        $emailAjaxEndpoints = new \WPPayForm\Pro\Classes\EmailNotification\EmailAjax();
        $emailAjaxEndpoints->register();

        $emailHandler = new \WPPayForm\Pro\Classes\EmailNotification\EmailHandler();
        $emailHandler->register();

        // Form Export Handler
        $xporter = new \WPPayForm\Pro\Classes\Export\Export();
        $xporter->registerEndpoints();

        // Init Pro Editor Componenets Here
        new \WPPayForm\Pro\Classes\Components\RecurringPaymentComponent();
        new \WPPayForm\Pro\Classes\Components\TaxItemComponent();
        new \WPPayForm\Pro\Classes\Components\TabularProductsComponent();
        new \WPPayForm\Pro\Classes\Components\FileUploadComponent();
        new \WPPayForm\Pro\Classes\Components\AddressFieldsComponent();
        new \WPPayForm\Pro\Classes\Components\MaskInputComponent();
        new \WPPayForm\Pro\Classes\Components\ConsentComponent();

        add_filter('wppayform/print_styles', function ($styles) {
           return [
               WPPAYFORM_URL.'assets/css/payforms-admin.css',
               WPPAYFORM_URL.'assets/css/payforms-print.css',
           ];
        }, 1, 1);

        // Custom CSS and JS
        $customCssJS = new \WPPayForm\Pro\Classes\CustomCSSJS();
        $customCssJS->registerEndpoints();

        // Default value Parser
        $formDefaultValueRenderer = new \WPPayForm\Pro\Classes\DefaultValueParser\FormDefaultValueRenderer();
        $formDefaultValueRenderer->register();


        add_shortcode('payform_user_submissions', function ($args) {
            $handler = new \WPPayForm\Pro\Classes\ProShortCodeHandler();
            return $handler->handleUserSubmissionShortCode($args);
        });

    }

    public function adminHooks()
    {
        // Schedule Settings
        $scheduleSettings = new \WPPayForm\Pro\Classes\SchedulingSettings();
        $scheduleSettings->register();

        // Recurring Info Init
        $recurringInfo = new \WPPayForm\Pro\Classes\RecurringInfo();
        $recurringInfo->register();

    }
}

$payformProInit = new WPPayFormProInit();
$payformProInit->boot();